export default () => (
    <div className="birthdayCard">
      <div className="cardFront">
        <h3 className="happy">HAPPY BIRTHDAY Sister!!</h3>
        <div className="balloons">
          <div className="balloonOne" />
          <div className="balloonTwo" />
          <div className="balloonThree" />
          <div className="balloonFour" />
        </div>
      </div>
      <div className="cardInside">
        <h3 className="back">HAPPY BIRTHDAY Laavanya!</h3>
        
        <p>
          Happy birthday!! Wishing you good health, happiness, and success in every step of your life.
          On your birthday, I wish nothing but the best for you!
        </p>
        <p className="name">Jiji</p>
      </div>
    </div>
  );
  